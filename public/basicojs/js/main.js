const btnEx1 = document.getElementById('btnEx1');

/* Exercicio 1 */
btnEx1.addEventListener('click', function(){
    document.getElementById('resultEx1').innerHTML = document.getElementById('inputEx1').value;
});

/* Exercicio 2 */
var inputEx2 = document.getElementById('inputEx2');

inputEx2.onkeyup = function(){
    document.getElementById('resultEx2').innerHTML = inputEx2.value;
}

/* Exercicio 3 */
var fruits = [
    {
        fruta: 'uva',
        imagem: 'https://s3.amazonaws.com/finecooking.s3.tauntonclud.com/app/uploads/2017/04/24172042/ING-grapes-2-thumb1x1.jpg',
    },
    {
        fruta: 'cereja',
        imagem: 'https://i.pinimg.com/originals/6d/86/64/6d8664c09617e381208b2163adfe409c.jpg',
    },
    {
        fruta: 'manga',
        imagem: 'http://img.sitemercado.com.br/produtos/cddd6b78efa43e4c514b2ac918c0da2214fa6cfbdc968b00b23dd6df2d2e3c36_full.jpg',
    },
    {
        fruta: 'kiwi',
        imagem: 'https://i.pinimg.com/originals/2d/51/20/2d5120b3340a7cf1c90f170d7ba63446.jpg',
    },
    {
        fruta: 'banana',
        imagem: 'https://4.imimg.com/data4/UL/DS/MY-13396088/cavendish-banana-500x500.jpg',
    },
];

const inputEx3 = document.getElementById('inputEx3');

inputEx3.onkeyup = function(){
    fruits.forEach(function(element) {
        if (inputEx3.value == element.fruta) {
            document.getElementById('resultEx3').src = element.imagem;
        } else {
            if (inputEx3.value == '') {
                document.getElementById('resultEx3').src = '';
            }
        }
    });
}

/* Exercicio 4 */
var frutas = [
    {
        fruta: 'uva',
        imagem: 'https://s3.amazonaws.com/finecooking.s3.tauntonclud.com/app/uploads/2017/04/24172042/ING-grapes-2-thumb1x1.jpg',
    },
    {
        fruta: 'cereja',
        imagem: 'https://i.pinimg.com/originals/6d/86/64/6d8664c09617e381208b2163adfe409c.jpg',
    },
    {
        fruta: 'manga',
        imagem: 'http://img.sitemercado.com.br/produtos/cddd6b78efa43e4c514b2ac918c0da2214fa6cfbdc968b00b23dd6df2d2e3c36_full.jpg',
    },
    {
        fruta: 'kiwi',
        imagem: 'https://i.pinimg.com/originals/2d/51/20/2d5120b3340a7cf1c90f170d7ba63446.jpg',
    },
    {
        fruta: 'banana',
        imagem: 'https://4.imimg.com/data4/UL/DS/MY-13396088/cavendish-banana-500x500.jpg',
    },
    {
        fruta: 'laranja',
        imagem: 'https://drwfxyu78e9uq.cloudfront.net/usercontent/olhafrutafresca/media/images/d4b19d0-fresh-orange-260584.jpg',
    },
    {
        fruta: 'mamao',
        imagem: 'https://cdn.awsli.com.br/600x450/766/766047/produto/30564919/0fa5387b1e.jpg',
    },
    {
        fruta: 'tomate',
        imagem: 'https://www.portaaportaorganicos.com/wp-content/uploads/2015/10/tomate.jpeg',
    },
    {
        fruta: 'melancia',
        imagem: 'https://quitandamoderna.com.br/wp-content/uploads/2017/08/melancia-500x500.jpg',
    },
    {
        fruta: 'melão',
        imagem: 'https://www.portaaportaorganicos.com/wp-content/uploads/2015/10/melaoamarelo.png',
    },
];

const btnEx4 = document.getElementById('btnEx4');
function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}
btnEx4.addEventListener('click', function(){
    var pos = randomNumber(1, frutas.length);
    document.getElementById('resultEx4').src = frutas[pos].imagem;
});

/* Exercicio 5 */
var frutasAuxiliar = frutas;
const btnEx5 = document.getElementById('btnEx5');

btnEx5.addEventListener('click', function(){
    var pos = randomNumber(1, frutasAuxiliar.length);
    document.getElementById('resultEx5').src = frutasAuxiliar[pos].imagem;
    frutasAuxiliar.splice(pos, 1);
    if (frutasAuxiliar.length == 0) {
        frutasAuxiliar = frutas;
        document.getElementById('info').innerHTML = 'Todas as frutas foram selecionadas.';
    }
});