var NUMERO_DE_ALUNOS = 31;
const btnSortear = document.getElementById('btnSortear');
const imgSorteado = document.getElementById('imgSorteado');
let sorteados = [], naoSorteados = [], historico = [];

(function() {
    for (let i = 1; i <= NUMERO_DE_ALUNOS; i++) naoSorteados.push(i);
})();

btnSortear.addEventListener('click', (event) => {
    animateSort(sortImg);
});

function animateSort(callback) {
    let delay = 1500;
    naoSorteados.forEach((image, index) => {
        delay = delay - 100;
        // console.log(image);
            setTimeout(() => {
            imgSorteado.src = `alunos/${image}.jpg`;
            imgSorteado.style.opacity = '0';
            imgSorteado.style.opacity = '1';
            if (index == naoSorteados.length - 1) {
                callback();
            }
        }, delay);
    });
}

function sortImg() {
    let sorteado;
    do {
        sorteado = sortear(1, NUMERO_DE_ALUNOS);
        // console.log('sorteado: ', sorteado);
    } while (sorteados.includes(sorteado));

    imgSorteado.src = `alunos/${sorteado}.jpg`;

    if (!sorteados.includes(sorteado)) {
        sorteados.push(sorteado);
    }
}

/* function buildAlunos() {
    var divAlunos = document.getElementById('alunos');
    for (let index = 1; index < NUMERO_DE_ALUNOS; index++) {
        var img = '<img class="avatar" alt="sorteado" src="alunos/' + index + '.jpg" />';
        divAlunos.innerHTML = divAlunos.innerHTML + img;
    }
}

buildAlunos(); */

btnSortear.addEventListener('click', function(){
    var sorteado = sortear(1, naoSorteados.length);
    console.log(sorteado);
    buildSorteado(sorteado);
    buildHistorico(sorteado);
});

function buildSorteado(sorteado) {
    imgSorteado.src = `alunos/${sorteado}.jpg`;
    // var divSorteado = document.getElementsByClassName('sorteado')[0];
    // divSorteado.innerHTML = '<img class="imgSorteada" alt="sorteado" src="alunos/' + sorteado + '.jpg" />';
}
function sortear(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
};

function sortearNumAleatorio(min, max) {
    var valor = Math.round(Math.random() * (max - min) - min);
    return valor;
}

function buildHistorico(sorteado) {
    historico.push(sorteado);
    console.log(sorteado);
    var divHistorico = document.getElementById('sorteados');
    var img = '<img class="avatar" alt="sorteado" src="alunos/' + sorteado + '.jpg" />';
    divHistorico.innerHTML = img + divHistorico.innerHTML;
    qntdSorteios();
}

var btnLimparHistorico = document.getElementById('btnLimparHistorico');
btnLimparHistorico.addEventListener('click', function(){
    historico = [];
    var divHistorico = document.getElementById('sorteados');
    divHistorico.innerHTML = '';
    qntdSorteios();
    var img = document.getElementsByClassName('sorteado')[0];
    img.innerHTML = '';
});

function qntdSorteios() {
    var qntd = historico.length;
    var texto = document.getElementById('qntdsorteios');
    texto.innerHTML = '';
    texto.innerHTML = qntd;
}
