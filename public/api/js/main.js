const list = document.getElementById('pokemonCard__list');
const searchBtn = document.getElementById('searchPoke');

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

searchBtn.addEventListener('click', e => {
   const input = document.getElementById('input');
   const API_URL = 'https://pokeapi.co/api/v2/pokemon/'+input.value.toLowerCase()+'/';

   fetch(API_URL)
   .then( data => data.json())
   .then( res => {
       const name = res.name;
       const id = res.id;
       const weight = res.weight;
       const xp = res.base_experience;
       const height = res.height;
       const order = res.order;

       document.getElementsByClassName('pokemonCard__img')[0].src = res.sprites.front_default;

       const types = res.types;
       let type = '<ul>';
       types.forEach(function(element){
           type += `<li>${element.type.name}</li>`;
       });
       type += '</ul>';

       const abilities = res.abilities;
       let ability = '<ul>';
       abilities.forEach(function(element){
        ability += `<li>${element.ability.name}</li>`;
       });
       ability += '</ul>';

       const moves = res.moves;
       let move = '<ul>';
       moves.forEach(function(element){
        move += `<li>${element.move.name}</li>`;
       });
       move += '</ul>';

       list.innerHTML = `
       <li>Order: ${order}</li>
       <li>Name: ${name.capitalize()}</li>
       <li>ID: ${id}</li>
       <li>Weight: ${weight}</li>
       <li>Height: ${height}</li>
       <li>Experience: ${xp}</li>
       <li>Types: ${type}</li>
       <li>Abilities: ${ability}</li>
       <li>Moves: ${move}</li>
       `;

       getVideo(name);
   });
});

function getVideo(pokemon){
    var idYoutube;
    const URL_TO_YOUTUBE_API = "https://www.googleapis.com/youtube/v3/search?part=snippet&q="+pokemon+"type=video&key=AIzaSyAqYHWWVbIQD_tw9MQknxw5gq-LccvmGTM";
    fetch(URL_TO_YOUTUBE_API)
    .then(data => data.json())
    .then(response => {
        idYoutube = response.items[0].id.videoId;
        var diVideo = document.getElementById('video');
        diVideo.innerHTML = '<iframe width="420" height="345" src="https://www.youtube.com/embed/'+idYoutube+'"</iframe>';
    })
    .catch(function(err){
        var diVideo = document.getElementById('video');
        diVideo.innerHTML = "<p>Não foi possível encontrar um vídeo, tente novamente mais tarde!";
    })
}